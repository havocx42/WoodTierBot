import asyncio
import random
import traceback
from datetime import datetime
from datetime import timedelta
from event import Event
import discord
import pytz

import persistance
import warriors
import wig
import config
import pdb

date_format = "%Y-%m-%d %H:%M"

client = discord.Client()

def remind_wig():
    now = datetime.now()
    now_epoch = wig.convert_to_wig_epoch(now)
    for w in persistance.wigs:
        not_answered = []
        if int(w.end) > int(now_epoch):
            answerwees = wig.list_answerees(w.results_url)
            for warrior in warriors.warriors:
                if warrior not in [warriors.lookup_by_alias(a) for a in answerwees]:
                    not_answered.append(warrior)
            if len(not_answered) > 0:
                message = "REMINDER:\n" + "\n".join(
                    [n.mention + " Please fill in the WiG " + w.results_url for n in not_answered])
                send_message_and_close(message)


async def send_message(message):
    await client.wait_until_ready()
    for server in client.servers:
        print(server.name)
        if server.name == config.discord_server:
            for channel in server.channels:
                if channel.name == config.discord_channel:
                    await client.send_message(channel, message)


def send_message_and_close(message):
    @client.event
    async def on_ready():
        for server in client.servers:
            print(server.name)
            if (server.name == config.discord_server):
                for channel in server.channels:
                    if (channel.name == config.discord_channel):
                        the_channel = channel
        await client.send_message(the_channel, message)
        await client.logout()

    print("Starting Discord client")

    client.run(config.discord_token)
    print("Discord client finished")
    client.close()


async def cmd_choose(message, options):
    if options[0].isdigit():
        num = int(options[0])
        options = options[1:]
    else:
        num = 5

    chosen = random.sample(options, num)
    await client.send_message(message.channel, ", ".join(chosen))


async def cmd_window(message, options):
    tmp = await client.send_message(message.channel, 'Processing...')
    answer = wig.get_windows()
    await client.edit_message(tmp, answer)


async def cmd_available(message, options):
    try:
        time = datetime.strptime(options[0] + " " + options[1], date_format)
    except ValueError as e:
        traceback.print_exc()
        await client.send_message(message.channel, 'Error: Expected date format is ' + date_format)
        return
    tmp = await client.send_message(message.channel, 'Processing...')
    available = wig.find_available(time)
    print(message.author.id)
    available_warriors = [warriors.lookup_by_alias(alias) for alias in available]
    await client.edit_message(tmp, str(len(available)) + " player(s) available: " + (
        ", ".join([w.name for w in available_warriors])))


async def cmd_event(message, options):
    if message.author.id != '86484456327634944':
        await client.send_message(message.channel, 'UNAUTHORIZED ACCESS')
        return
    if len(options) > 2:
        event_name = " ".join(options[2:])
    else:
        event_name = None
    try:
        date = options[0]
        time = options[1]
        time = pytz.timezone('Europe/Dublin').localize(
            datetime.strptime(date + " " + time, "%Y-%m-%d %H:%M", ))
    except ValueError as e:
        traceback.print_exc()
        await client.send_message(message.channel, 'Error: Expected date format is ' + date_format)
        return
    fmt = '%A %d %b %I:%M %p %Z'
    tmp = await client.send_message(message.channel, 'Processing for event on ' + time.strftime(fmt) + '...')
    available = wig.find_available(time)
    if available is None:
        await client.edit_message(tmp, "Error: Unable to find WiG for that date")
        return
    available_warriors = [warriors.lookup_by_alias(alias) for alias in available]
    if len(available_warriors) < 5:
        await client.edit_message(tmp, "Not enough team members for that date. Player(s) available: " + (
            ", ".join([w.name for w in available_warriors])))
    else:
        chosen = random.sample(available_warriors, 5)
        message = ""
        if event_name is not None:
            message = "For the event '" + event_name + "' on"
        else:
            message = "For the event on "
        message += time.strftime(
            fmt) + " the following Waffle Tier Warriors have been selected:\n\n"
        message += "\n\n ".join([c.mention + " This event is at " + time.astimezone(
            c.timezone).strftime(fmt) + " in your timezone" for c in chosen])
        await client.edit_message(tmp, message)
        persistance.save_event(Event(time, available_warriors, tmp, event_name))


async def cmd_users(message, options):
    for i in message.server.members:
        print(str(i.name) + " " + str(i.id))


def create_reminder_message(event):
    fmt = '%A %d %b %I:%M %p %Z'
    message = "Event reminder for " + event.datetime.strftime(fmt) + ": \n"
    message += ", ".join([w.mention for w in event.warriors])
    if event.name is not None:
        message += "\n The event '" + event.name + "' that you are scheduled to play in starts in less than an hour!"
    else:
        message += "\n An event that you scheduled to play in starts in less than an hour!\n"
    return message


async def reminder_loop():
    while True:
        time_now = pytz.timezone(config.local_timezone).localize(datetime.now())
        for event in persistance.events:
            if (not event.reminder_sent) and event.datetime - time_now < timedelta(hours=1):  # less than 1 hour away and not reminded
                await send_message(create_reminder_message(event))
                event.reminder_sent = True
                persistance.save_event(event)
        await asyncio.sleep(60)


async def run_loop():
    await client.login(config.discord_token)
    tasks = [asyncio.ensure_future(client.connect()), asyncio.ensure_future(reminder_loop())]
    await asyncio.wait(tasks)


def listen():
    @client.event
    async def on_message(message):
        if message.content.startswith("!"):
            print('Message: "' + message.content + '"')
            tokens = message.content.strip().split(' ')
            command = tokens[0]
            options = tokens[1:]
            if command == '!choose':
                await cmd_choose(message, options)

            elif command == '!available':
                await cmd_available(message, options)

            elif command == '!event':
                await cmd_event(message, options)

            elif command == '!users':
                await cmd_users(message, options)

            elif command == "!window":
                await cmd_window(message, options)

    print("Starting Discord client")
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(run_loop())
    except:
        loop.run_until_complete(client.logout())
    finally:
        loop.close()
    print("Discord client finished")
    return None
